# LIS 3781

## Joshua Chanin

### Project 2 Requirements:


1. Install MongoDb on Ampps
2. Run MongoDb on Ampps
3. Navigate to MongoDB bin through Command Prompt
4. Import test database with it's collections

![P2-CMD](img/mongo1.png)


![P2-CMD](img/mongo2.png)


![P2-CMD](img/mongo3.png)

