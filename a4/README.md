# LIS 3781

## Joshua Chanin

### Assignment 4 Requirements:


1. Code to create tables
2. Code for data inserts
3. Execute code in Oracle
4. Create ERD Diagram using Oracle
5. Screenshot the ERD
6. Create README file for assignment 4
7. Push files to the lis 3781 repository

![A4-ERD](img/Assn4ERDNEW.png)
ERD Screenshot