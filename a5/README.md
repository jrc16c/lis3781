# LIS 3781

## Joshua Chanin

### Assignment 5 Requirements:


1. Create tables 
2. Insert data 
3. Create ERD in Oracle
4. Screenshot of the ERD
5. Populated tables in Putty
6. Screenshot of populated tables
7. Create README file for assignment 5
8. Push files to the lis 3781 repository

![A5-ERD](img/a5erd.png)
ERD Screenshot