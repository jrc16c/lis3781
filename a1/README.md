# LIS 3781

## Joshua Chanin

### Assignment 1 Requirements:



1. Create tables (dependent, employee, job, plan, benefit, emp_hist)
2. Make ERD diagram
3. Add insert data
4. Add relationships between tables
5. Make sure ERD forward engineers
6. Screenshot ERD
7. Push to lis 3781 repository

![A1-ERD](img/erda1.png)