# LIS 3781

## Joshua Chanin

### Assignment 3 Requirements:


1. Create tables (customer, commodity, & order)
2. Screenshot of the code
3. Create and populate tables in Oracle
4. Screenshot of populated tables
5. Create README file for assignment 3
6. Push files to the lis 3781 repository

![A3-code1](img/codea3a.png)
Part one of code

![A3-code2](img/code3b.png)
Part two of code

![A3-oracle1](img/oraclecus.png)
Part 1 of customer table populated

![A3-oracle2](img/oraclecusb.png)
Part 2 of customer table populated

![A3-oracle3](img/oraclecomord.png)
commodity and "order" table populated