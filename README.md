# LIS 3781
## Joshua Chanin

## 3 Rules of Database Design
1.	All tables must have a primary key and the primary key cannot be null.
2.	Foreign key must match value of primary key in the parent table or may be null depending upon business rules.
3.	Single themed tables.

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Create tables
    - Create ERD

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create tables
    - Populate tables
    - Create repository

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Code tables
    - Populate tables in Oracle
    - Push files to repository

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create table on Workbench
    - Populate tables in Putty
    - Push screenshots to repository

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Code to create tables and insert data
    - Paste code in Oracle and execute
    - Create ERD in Oracle

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Code to create tables and insert data (build from A4)
    - Paste code in Oracle and execute
    - Create ERD in Oracle

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install MongoDb on Ampps
    - Run MongoDb on Ampps
    - Navigate to MongoDB bin through Command Prompt
    - Import test database with it's collections


https://bitbucket.org/jrc16c/lis3781/src/master/

