# LIS 3781

## Joshua Chanin

### Assignment 2 Requirements:


1. Create tables (company & customer)
2. Populate tables usin SSH
3. Screenshot tables and populated data
4. Create lis 3781 repository
5. Push files to the lis 3781 repository

![A2](img/a2a.png)
![A2](img/a2b.png)
![A2](img/del.png)

#### README.md file should include the following items:


#### Git commands w/short descriptions:

1. git init - create a new local repository
2. gir status - List the files you've changed and those you still need to add or commit
3. git add - add file
4. git commit -m - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git remote -v - List all currently configured remote repositories




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
