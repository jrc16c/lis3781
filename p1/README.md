# LIS 3781

## Joshua Chanin

### Project 1 Requirements:


1. Create tables (11 of them)
2. Screenshot of the ERD
3. Create tables in Workbench
4. Populated tables in Putty
5. Screenshot of populated tables
6. Create README file for assignment 3
7. Push files to the lis 3781 repository

![P1-ERD](img/p1erd.png)
ERD Screenshot

![P1-Populated1](img/part1.png)
Part one of populated tables

![P1-Populated2](img/part2.png)
Part two of populated tables

![P1-Populated3](img/part3.png)
Part three of populated tables

![P1-Populated4](img/part4.png)
Part four of populated tables

![P1-Populated5](img/part5.png)
Part five of populated tables

![P1-Populated6](img/part6.png)
Part six of populated tables